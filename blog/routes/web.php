<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function() {
    echo "<a href=". route('contactos') .">Contacto</a></br>";
    echo "<a href=". route('contactos') .">Contacto</a></br>";
    echo "<a href=". route('contactos') .">Contacto</a></br>";
    echo "<a href=". route('contactos') .">Contacto</a></br>";
});


Route::get('contactame',['as' => 'contactos' , function(){
    return "Sección de contacto";
}]);


Route::get('/hometwo/{name?}', function($name = "invitado") {
    //Ejemplo https://localhost/larabelstore/blog/public/hometwo/luis
    return "<h1>Hola {$name}</h1>";
})->where('name',"[A-Za-z]+");//con ->where('nombreVariable',"[expresión]+") validamos que solo se ingresen caracteres



//Esto es una ruta
Route::get('/shop', function() {
    return "<h1>SHOP</h1>";
});

Route::get('cursos/create', function () {
    return "Cursos de php";
});

Route::get('cursos/{curso}', function ($curso) {
    //return "Curso de " . $curso;
    return "Curso de {$curso}";
});



